<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="css/styles.css" rel="stylesheet" type="text/css" media="screen"/>
        <title>FileXChange</title>
    </head>
    <body>
    	<div id="postfile">
        	<h1>Share a file</h1>
        	<c:if test="${fileurl != null}">
    			<div id="info">
    				Your file has been upload.<br/>
    				You can download it during 30 days at url : <br/>
    				<a href="${fileurl}">${fileurl}</a> 
    			</div>
    		</c:if>
        	<form action="postfile" method="post" enctype="multipart/form-data">
        		<fieldset>
        			<label for="data">File</label>
        			<input type="file" id="data" name="data"/>
        		
        			<label for="owner">Email</label>
        			<input type="text" id="owner" name="owner" placeholder="Enter your email"/>
        			
        			<label for="receivers">Receivers</label>
        			<input type="text" id="receivers" name="receivers" placeholder="Enter receivers emails"/>
        			
        			<label for="message">Message</label>
        			<textarea id="message" name="message" placeholder="Your message here"></textarea>
        			
        			<input type="submit" value="Send File"/> 
        		</fieldset>
	        </form>
        </div>
    </body>
</html>
