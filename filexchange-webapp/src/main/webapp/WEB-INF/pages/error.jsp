<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="css/styles.css" rel="stylesheet" type="text/css" media="screen"/>
        <title>FileXChange - Error</title>
    </head>
    <body>
    	<div id="error">
        	<h1>Oops an error occurred</h1>
        	<div id="message">${error_message}</div>
        	<c:if test="${error_body != null}">
        		<div id="body">${error_body}</div>
    		</c:if>
        </div>
    </body>
</html>
