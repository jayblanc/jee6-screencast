<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>

<!DOCTYPE html>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="css/styles.css" rel="stylesheet" type="text/css" media="screen"/>
        <title>FileXChange - Admin</title>
    </head>
    <body>
    	<div id="files">
        	<h1>Availables files</h1>
        	<display:table name="files">
        		<display:column property="name"/>
  				<display:column property="owner" />
  				<display:column property="creation" />
  				<display:column property="lastdownload" />
  				<display:column property="nbdownloads" title="Downloads"/>
  				<display:column property="receivers"/>
        	</display:table>
        </div>
    </body>
</html>
