package fr.loria.filexchange.servlet;

import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.HttpConstraint;
import javax.servlet.annotation.ServletSecurity;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.loria.filexchange.entity.FileItem;
import fr.loria.filexchange.service.FileService;
import fr.loria.filexchange.service.FileServiceAdmin;
import fr.loria.filexchange.service.FileServiceException;

@WebServlet(name="AdminFilesServlet", urlPatterns="/adminfiles")
@ServletSecurity(value = @HttpConstraint(rolesAllowed = "admin"))
public class AdminFilesServlet  extends HttpServlet {

	private static final long serialVersionUID = -457189465123454846L;
	private static final Logger logger = Logger.getLogger(AdminFilesServlet.class.getName());
	
	@EJB
	private FileServiceAdmin aservice;
	@EJB
	private FileService service;
	
	@Override
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		logger.log(Level.INFO, "AdminFiles request received");
		
		if ( request.getParameter("id") != null ) {
			String id = request.getParameter("id");
			try {
				service.deleteFile(id);
			} catch (FileServiceException e) {
				request.setAttribute("error_message", e.getMessage());
				request.setAttribute("error_body", e.getStackTrace());
			}
		}
		try {
			List<FileItem> files = aservice.listFiles();
			request.setAttribute("files", files);
			request.getRequestDispatcher("/WEB-INF/pages/admin.jsp").forward(request,response);
		} catch (FileServiceException e) {
			request.setAttribute("error_message", e.getMessage());
			request.setAttribute("error_body", e.getStackTrace());
		}
	}
}
