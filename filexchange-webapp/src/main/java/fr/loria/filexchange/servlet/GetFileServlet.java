package fr.loria.filexchange.servlet;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.loria.filexchange.entity.FileItem;
import fr.loria.filexchange.service.FileService;
import fr.loria.filexchange.service.FileServiceException;

//urlPatterns="/[a-f0-9]{32}"
@WebServlet(name="GetFileServlet", urlPatterns="/getfile")
public class GetFileServlet extends HttpServlet {

	private static final long serialVersionUID = -457189465466814846L;
	private static final Logger logger = Logger.getLogger(GetFileServlet.class.getName());
	
	@EJB
	private FileService service;
	
	@Override
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		logger.log(Level.INFO, "GetFile request received");
		
		if ( request.getParameter("id") != null ) {
			String id = request.getParameter("id");
			try {
				FileItem item = service.getFile(id);
				if ( item.getLength() != null ) {
					response.setContentLength(item.getLength().intValue());
				}
				if ( item.getType() != null ) {
					response.setContentType(item.getType());
				}
				if ( item.getName() != null ) {
					response.setHeader( "Content-Disposition", "attachment; filename=\"" + item.getName() + "\"" );
				}
				byte[] data = service.getFileContent(id);
				ByteArrayInputStream bais = new ByteArrayInputStream(data);
				ServletOutputStream sos = response.getOutputStream();
				byte[] buffer = new byte[1024];
				int nbread = -1;
				while ( (nbread = bais.read(buffer)) > 0 ) {
					sos.write(buffer, 0, nbread);
				}
				sos.flush();
				return;
			} catch (FileServiceException e) {
				request.setAttribute("error_message", e.getMessage());
				request.setAttribute("error_body", e.getStackTrace());
			}
		} else {
			request.setAttribute("error_message", "You did not provide any file id.");
		}
		request.getRequestDispatcher("/WEB-INF/pages/error.jsp").forward(request,response);
	}
	
}

