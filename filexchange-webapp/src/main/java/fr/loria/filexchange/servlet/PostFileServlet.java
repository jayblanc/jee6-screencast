package fr.loria.filexchange.servlet;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.FileItemIterator;
import org.apache.commons.fileupload.FileItemStream;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.fileupload.util.Streams;

import fr.loria.filexchange.service.FileService;

@WebServlet(name="PostFileServlet", urlPatterns="/postfile")
public class PostFileServlet extends HttpServlet {

	private static final long serialVersionUID = -4571386567066814846L;
	private static final Logger logger = Logger.getLogger(PostFileServlet.class.getName());
	private static FileItemFactory factory = new DiskFileItemFactory();
	
	@EJB
	private FileService service;
	
	
	@Override
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		logger.log(Level.INFO, "PostFile request received");
		
		String owner = "";
		String receivers = "";
		String message = "";
		String contentname = "";
		String contenttype = "";
		byte[] content = null;
		
		boolean isMultipart = ServletFileUpload.isMultipartContent(request);
		if ( isMultipart ) {
			try {
				ServletFileUpload upload = new ServletFileUpload();
				FileItemIterator iter = upload.getItemIterator(request);
				while ( iter.hasNext() ) {
				    FileItemStream item = iter.next();
				    String name = item.getFieldName();
				    InputStream stream = item.openStream();
				    if (item.isFormField()) {
				    	if ( name.equals("owner") ) {
							owner = Streams.asString(stream);
						} 
						if ( name.equals("receivers") ) {
							receivers = Streams.asString(stream);
						} 
						if ( name.equals("message") ) {
							message = Streams.asString(stream);
						}
				    } else {
				    	contenttype = item.getContentType();
				    	contentname = item.getName();
				    	
				    	InputStream is = item.openStream();
				    	ByteArrayOutputStream baos = new ByteArrayOutputStream();
				    	byte[] buffer = new byte[1024];
				    	int nbread = 0;
				    	while ( (nbread = is.read(buffer)) >= 0) {
				    		baos.write(buffer, 0, nbread);
				    	}
				    	content = baos.toByteArray();
				    }
				}
				String id = service.postFile(owner, Arrays.asList(receivers.split(",")), message, contentname, content);
				String fileUrl =  request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath() + "/getfile?id=" + id;
				request.setAttribute("fileurl", fileUrl);
				request.getRequestDispatcher("/index.jsp").forward(request,response);
				return;
			} catch (Exception e) {
				request.setAttribute("error_message", e.getMessage());
				request.setAttribute("error_body", e.getStackTrace());
			}
		} else {
			request.setAttribute("error_message", "This url is only accessible using multipart form requests");
		}
		request.getRequestDispatcher("/WEB-INF/pages/error.jsp").forward(request,response);
	}


}
