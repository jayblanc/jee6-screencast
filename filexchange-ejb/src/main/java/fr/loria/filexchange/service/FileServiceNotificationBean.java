package fr.loria.filexchange.service;

import java.io.UnsupportedEncodingException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.Resource;
import javax.ejb.MessageDriven;
import javax.jms.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.Message.RecipientType;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

@MessageDriven(mappedName="filexchangeTopic")
public class FileServiceNotificationBean {
	
	private static Logger logger = Logger.getLogger(FileServiceNotificationBean.class.getName());
	
	//TODO change this by adding application configuration file.
	private String GET_FILE_URL_PREFIX = "http://corosif.loria.fr:8080/filexchange/getfile?id=";
	
	@Resource(name = "mail/filexchange")  
	private Session session;
	
	public void onMessage(Message message) {        
	    logger.log(Level.INFO, "Notification message received : " + message.toString());
	    try {
			String action = message.getStringProperty("action");
			if ( action.equals("post") ) {
				String id = message.getStringProperty("id");
				String owner = message.getStringProperty("owner");
				String receivers = message.getStringProperty("receivers");
				String msg = message.getStringProperty("message");
				for (String receiver : receivers.split(",")) {
					notifyReceiver(receiver, id, msg);
				}
				notifyOwner(owner, id);
			}
		} catch (Exception e) {
			logger.log(Level.SEVERE, "Error during message reception", e);
		} 
	}
	
	private void notifyOwner(String owner, String id) throws MessagingException, UnsupportedEncodingException {
		javax.mail.Message msg = new MimeMessage(session);  
		msg.setSubject("Your file has been received");  
		msg.setRecipient(RecipientType.TO,new InternetAddress(owner));  
		msg.setFrom(new InternetAddress("admin@filexchange.org","FileXChange"));  
		msg.setContent("Hi, this mail confirm the upload of your file. The file will be accessible at url : " 
				+ GET_FILE_URL_PREFIX + id, "text/html");
		Transport.send(msg);  
	}
	
	private void notifyReceiver(String receiver, String id, String message) throws MessagingException, UnsupportedEncodingException {
		javax.mail.Message msg = new MimeMessage(session);  
		msg.setSubject("Notification");
		msg.setRecipient(RecipientType.TO,new InternetAddress(receiver));
		msg.setFrom(new InternetAddress("admin@filexchange.org","FileXChange"));  
		msg.setContent("Hi, a file has been uploaded for you and is accessible at url : <br/><br/>" 
				+ GET_FILE_URL_PREFIX + id + "<br/><br/>" 
				+ "The sender lets you a message :<br/><br/>" + message, "text/html");
		Transport.send(msg);  
	}

}
