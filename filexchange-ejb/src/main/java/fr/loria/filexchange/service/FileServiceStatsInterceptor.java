package fr.loria.filexchange.service;

import java.util.logging.Logger;

import javax.interceptor.AroundInvoke;
import javax.interceptor.InvocationContext;

public class FileServiceStatsInterceptor {
	
	private Logger logger = Logger.getLogger(FileServiceStatsInterceptor.class.getName());
	
	private static int downloads = 0;
	private static int uploads = 0;

	@AroundInvoke
	public Object collectStats(InvocationContext ic) throws Exception {
		logger.entering(ic.getTarget().toString(), ic.getMethod().getName());
		try {
			Object obj =  ic.proceed();
			if ( ic.getMethod().getName().equals("getFileContent") ) {
				downloads++;
			}
			if ( ic.getMethod().getName().equals("postFile") ) {
				uploads++;
			}
			return obj;
		} finally {
			logger.exiting(ic.getTarget().toString(), ic.getMethod().getName());
		}
	}
	
	public static int getTotalFilesDownloads() {
		return downloads;
	}
	
	public static int getTotalFilesUploads() {
		return uploads;
	}
}
