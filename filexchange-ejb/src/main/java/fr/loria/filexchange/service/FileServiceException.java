package fr.loria.filexchange.service;

public class FileServiceException extends Exception {

	private static final long serialVersionUID = 5755811732253531020L;

	public FileServiceException() {
		super();
	}

	public FileServiceException(String arg0, Throwable arg1) {
		super(arg0, arg1);
	}

	public FileServiceException(String arg0) {
		super(arg0);
	}

	public FileServiceException(Throwable arg0) {
		super(arg0);
	}

}
