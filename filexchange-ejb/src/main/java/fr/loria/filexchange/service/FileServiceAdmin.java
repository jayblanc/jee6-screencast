package fr.loria.filexchange.service;

import java.util.List;

import javax.ejb.Local;

import fr.loria.filexchange.entity.FileItem;

@Local
public interface FileServiceAdmin {
	
	public List<FileItem> listFiles() throws FileServiceException;
	
}
