package fr.loria.filexchange.service;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.Resource;
import javax.annotation.security.RolesAllowed;
import javax.ejb.Schedule;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.interceptor.Interceptors;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Session;
import javax.jms.Topic;
import javax.jms.TopicConnection;
import javax.jms.TopicConnectionFactory;
import javax.jws.WebService;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TemporalType;

import fr.loria.filexchange.entity.FileContent;
import fr.loria.filexchange.entity.FileItem;

@Stateless
@WebService(endpointInterface="fr.loria.filexchange.service.FileServiceStats")
public class FileServiceBean implements FileService, FileServiceAdmin, FileServiceStats {
	
	private static Logger logger = Logger.getLogger(FileServiceBean.class.getName());
	
	@PersistenceContext(unitName="filexchange-pu")
	private EntityManager em;
	@Resource(name="connFactory", mappedName="filexchangeConnFactory")
	private TopicConnectionFactory tFactory;
	@Resource(name="topic", mappedName="filexchangeTopic")
	private Topic topic;
	@Resource
	private SessionContext ctx;
	
	@Override
	@Interceptors(FileServiceStatsInterceptor.class)
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public String postFile(String owner, List<String> receivers, String message, String name, byte[] data) throws FileServiceException {
		logger.log(Level.INFO, "Post File called");
		try {
			String id = UUID.randomUUID().toString().replaceAll("-", "");
			FileItem file = new FileItem();
			file.setId(id);
			file.setOwner(owner);
			file.setReceivers(receivers);
			file.setMessage(message);
			file.setName(name);
			FileContent content = new FileContent();
			content.setData(data);
			file.setContent(content);
			em.persist(file);
			HashMap<String, String> params = new HashMap <String, String> ();
			params.put("id", id);
			params.put("owner", owner);
			StringBuffer buffer = new StringBuffer();
			for (String receiver : receivers) {
				buffer.append(receiver + ",");
			}
			buffer.setLength(buffer.length()-1);
			params.put("receivers", buffer.toString());
			params.put("message", message);
			sendNotification("post", params);
			return id;
		} catch ( Exception e ) {
			logger.log(Level.SEVERE, "An error occured during posting file", e);
			throw new FileServiceException(e);
		}
	}
	
	@Override
	@RolesAllowed(value={"admin"})
	@TransactionAttribute(TransactionAttributeType.SUPPORTS)
	public List<FileItem> listFiles() throws FileServiceException {
		logger.log(Level.INFO, "List All Files called");
		try {
			List<FileItem> items = em.createNamedQuery("listAllFiles").getResultList();
			HashMap<String, String> params = new HashMap <String, String> ();
			sendNotification("list", params);
			return items;
		} catch ( Exception e ) {
			logger.log(Level.SEVERE, "An error occured during listing files", e);
			throw new FileServiceException(e);
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void deleteFile(String id) throws FileServiceException {
		logger.log(Level.INFO, "Delete File called");
		try {
			FileItem item = em.find(FileItem.class, id);
			if ( item == null ) {
				throw new FileServiceException("Unable to delete file with id '" + id + "' : file does not exists");
			} else {
				em.remove(item);
				HashMap<String, String> params = new HashMap <String, String> ();
				params.put("id", id);
				sendNotification("delete", params);
			}
		} catch ( Exception e ) {
			logger.log(Level.SEVERE, "An error occured during deleting file", e);
			throw new FileServiceException(e);
		}	
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.SUPPORTS)
	public FileItem getFile(String id) throws FileServiceException {
		logger.log(Level.INFO, "Get File called");
		try {
			FileItem item = em.find(FileItem.class, id);
			if ( item == null ) {
				throw new FileServiceException("Unable to retrieve file with id '" + id + "' : file does not exists");
			}
			HashMap<String, String> params = new HashMap <String, String> ();
			params.put("id", id);
			sendNotification("get", params);
			return item;
		} catch ( Exception e ) {
			logger.log(Level.SEVERE, "An error occured during getting file", e);
			throw new FileServiceException(e);
		}
	}

	@Override
	@Interceptors(FileServiceStatsInterceptor.class)
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public byte[] getFileContent(String id) throws FileServiceException {
		logger.log(Level.INFO, "Get File Content called");
		try {
			FileItem item = em.find(FileItem.class, id);
			if ( item == null ) {
				throw new FileServiceException("Unable to retrieve file with id '" + id + "' : file does not exists");
			}
			item.setLastdownload(new Date());
			item.setNbdownloads(item.getNbdownloads()+1);
			em.merge(item);
			FileContent content = item.getContent();
			HashMap<String, String> params = new HashMap <String, String> ();
			params.put("id", id);
			sendNotification("get-data", params);
			return content.getData();
		} catch ( Exception e ) {
			logger.log(Level.SEVERE, "An error occured during getting file data", e);
			throw new FileServiceException(e);
		}	
	}
	
	@Override
	@TransactionAttribute(TransactionAttributeType.SUPPORTS)
	public int getTotalDownloads() throws FileServiceException {
		return FileServiceStatsInterceptor.getTotalFilesDownloads();
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.SUPPORTS)
	public int getTotalUploads() throws FileServiceException {
		return FileServiceStatsInterceptor.getTotalFilesUploads();
	}
	
	@Schedule(minute="*/10", hour="*")
	public void cleanExpiredFiles() {
		logger.log(Level.INFO, "Clean Expired File called");
		Date limit = new Date(System.currentTimeMillis() - 600000);
		List<FileItem> items = em.createNamedQuery("findExpiredFiles").setParameter("limit", limit,  TemporalType.TIMESTAMP).getResultList();
		logger.log(Level.INFO, items.size() + " files expired found, cleaning...");
		for (FileItem item : items) {
			em.remove(item);
		}
	}
	
	private void sendNotification(String action, HashMap<String, String> params) throws JMSException {
		TopicConnection conn = tFactory.createTopicConnection();
		Session session = conn.createSession(false, Session.AUTO_ACKNOWLEDGE);
		Message msg = session.createMessage();
		msg.setStringProperty("action", action);
		for (String key : params.keySet()) {
			msg.setStringProperty(key, params.get(key));
		}
		session.createProducer(topic).send(msg);
	}

}
