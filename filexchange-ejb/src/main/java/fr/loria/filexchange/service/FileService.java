package fr.loria.filexchange.service;

import java.util.List;

import javax.ejb.Remote;

import fr.loria.filexchange.entity.FileItem;

@Remote
public interface FileService {
	
	public String postFile(String owner, List<String> receivers, String message, String name, byte[] content) throws FileServiceException;
	
	public FileItem getFile(String id) throws FileServiceException;
	
	public byte[] getFileContent(String id) throws FileServiceException;
	
	public void deleteFile(String id) throws FileServiceException;
	
}
