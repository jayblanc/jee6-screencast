package fr.loria.filexchange.service;

import javax.jws.WebService;

@WebService
public interface FileServiceStats {
	
	public int getTotalUploads() throws FileServiceException;
	
	public int getTotalDownloads() throws FileServiceException;

}
