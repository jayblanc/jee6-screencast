package fr.loria.filexchange.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@NamedQueries({ 
	@NamedQuery(name = "listAllFiles", query = "SELECT fi FROM FileItem fi;"),
	@NamedQuery(name = "countAllFiles", query = "SELECT count(fi) FROM FileItem fi;"),
	@NamedQuery(name = "findExpiredFiles", query = "SELECT fi FROM FileItem fi WHERE fi.lastdownload < :limit;") 
})
public class FileItem implements Serializable {

	@Id
	private String id;
	private String name;
	private String type;
	private Long length;
	private Long nbdownloads;
	private String owner;
	private List<String> receivers;
	@OneToOne(cascade={CascadeType.PERSIST, CascadeType.REMOVE})
	public FileContent content;
	@Column(length = 2000)
	private String message;
	@Temporal(TemporalType.TIMESTAMP)
	private Date creation;
	@Temporal(TemporalType.TIMESTAMP)
	private Date lastdownload;
	
	public FileItem() {
		nbdownloads = 0l;
		creation = new Date();
		lastdownload = creation;
	}

	public String getOwner() {
		return owner;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}

	public List<String> getReceivers() {
		return receivers;
	}

	public void setReceivers(List<String> receivers) {
		this.receivers = receivers;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Date getLastdownload() {
		return lastdownload;
	}

	public void setLastdownload(Date lastdownload) {
		this.lastdownload = lastdownload;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Long getLength() {
		return length;
	}

	public void setLength(Long length) {
		this.length = length;
	}

	public Long getNbdownloads() {
		return nbdownloads;
	}

	public void setNbdownloads(Long nbdownloads) {
		this.nbdownloads = nbdownloads;
	}

	public FileContent getContent() {
		return content;
	}

	public void setContent(FileContent content) {
		this.content = content;
	}

	public void setCreation(Date creation) {
		this.creation = creation;
	}

	public Date getCreation() {
		return creation;
	}

}
